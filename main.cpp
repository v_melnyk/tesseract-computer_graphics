//global includes
#include <math.h>
#include <GL/glut.h>

//local includes
#include "constants.h"
#include "tesseract.h"


float transformation = TRANSFORMATION_DEFAULT;
bool animation = ANIMATION_DEFAULT;

void setup(float t, float *koof1, float *koof2, float *x){
    float d = -(OUTER_CUBE - INNER_CUBE) / 2.0f;

    t = in_out_f(t);

    *koof1 = scaling(sin(M_PI * t - M_PI / 2.0f));

    if (t < 0.25) {
        *x = d * t * 4.0f;
        *koof2 = 0.0f;
    } else {
        *x = d;
        *koof2 = scaling(sin((M_PI + 1.0f) * (t - 0.25) - M_PI / 2.0f));
    }
}

void render(){
    int i, j;
    float outer= OUTER_CUBE;
    float inner = INNER_CUBE;
    float koof1, koof2, x;
    
    setup(transformation, &koof1, &koof2, &x);

    //MATRIX OF VERTICES
    float vertices[16][3] = {
        {mutate_s(+outer, +inner, koof1), mutate_s(+outer, +inner, koof1), mutate_s(+outer, +inner, koof1)},
        {mutate_s(+outer, +inner, koof1), mutate_s(+outer, +inner, koof1), mutate_s(-outer, -inner, koof1)},
        {mutate_s(+outer, +inner, koof1), mutate_s(-outer, -inner, koof1), mutate_s(+outer, +inner, koof1)},
        {mutate_s(+outer, +inner, koof1), mutate_s(-outer, -inner, koof1), mutate_s(-outer, -inner, koof1)},
        {mutate_s(-outer, +outer, koof1), +outer, +outer},
        {mutate_s(-outer, +outer, koof1), +outer, -outer},
        {mutate_s(-outer, +outer, koof1), -outer, +outer},
        {mutate_s(-outer, +outer, koof1), -outer, -outer},
        {mutate_x(+inner, -inner, koof2, x), +inner, +inner},
        {mutate_x(+inner, -inner, koof2, x), +inner, -inner},
        {mutate_x(+inner, -inner, koof2, x), -inner, +inner},
        {mutate_x(+inner, -inner, koof2, x), -inner, -inner},
        {mutate_x(-inner, -outer, koof2, x), mutate_s(+inner, +outer, koof2), mutate_s(+inner, +outer, koof2)},
        {mutate_x(-inner, -outer, koof2, x), mutate_s(+inner, +outer, koof2), mutate_s(-inner, -outer, koof2)},
        {mutate_x(-inner, -outer, koof2, x), mutate_s(-inner, -outer, koof2), mutate_s(+inner, +outer, koof2)},
        {mutate_x(-inner, -outer, koof2, x), mutate_s(-inner, -outer, koof2), mutate_s(-inner, -outer, koof2)},
    };

    //MATRIX OF EDGES
    int edges[34][2] = {
        {0, 1},
        {0, 2},
        {0, 4},
        {0, 8},
        {1, 3},
        {1, 5},
        {1, 9},
        {2, 3},
        {2, 6},
        {2, 10},
        {3, 7},
        {3, 11},
        {4, 5},
        {4, 6},
        {4, 12},
        {5, 7},
        {5, 13},
        {6, 7},
        {6, 14},
        {7, 15},
        {8, 9},
        {8, 10},
        {8, 12},
        {9, 11},
        {9, 13},
        {10, 11},
        {10, 14},
        {11, 15},
        {12, 13},
        {12, 14},
        {13, 15},
        {14, 15},
    };

    //DRAWING
    glColor4f(0.0f, 0.8f, 0.2f, 1.0f);
    glLineWidth(3.5f);

    for (i = 0; i < 34; ++i) {
        glBegin(GL_LINES);
            //every two points
            for (j = 0; j < 2; ++j) {
              glVertex3fv(vertices[edges[i][j]]);
            }
        glEnd();
    }

    glDisable(GL_BLEND);
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (animation) {
        transformation = animate(transformation, 1);
    }

    glPushMatrix();
    render();
    glPopMatrix();

    glFlush();
    glutSwapBuffers();
    glutPostRedisplay();
}

void init()
{
    float aspect_ratio = WINDOW_X / WINDOW_Y;

    glutInitWindowPosition(0, 0);
    glutInitWindowSize(WINDOW_X, WINDOW_Y);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutCreateWindow("Tesseract(4D Hypercube)");

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    gluPerspective(70.0f, aspect_ratio, 1.0f, 100.0f);

    gluLookAt(
        0.5f, 
        1.5f,
        2.5f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        1.0f,
        0.0f
    );

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LINE_SMOOTH);
}

void input(unsigned char key, int x, int y){
    switch (key) {
        case 'a':
            animation = toggle(animation);
            break;
        case 27:
            exit(0);
    }
    glutPostRedisplay();
}


int main(int argc, char** argv){
  
  glutInit(&argc, argv);

  init();

  glutDisplayFunc(display);
  glutKeyboardFunc(input);
  glutMainLoop();

  return 0;
}

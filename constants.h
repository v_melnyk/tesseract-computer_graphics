//defined constants

#define OUTER_CUBE 0.5f
#define INNER_CUBE 0.2f

#define WINDOW_X  1000.0f
#define WINDOW_Y 1000.0f

#define TRANSFORMATION_DEFAULT 0.0f
#define TRANSFORMATION_SPEED 0.005f

#define ANIMATION_DEFAULT true
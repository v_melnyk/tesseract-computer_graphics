#include <math.h>

#include "tesseract.h"
#include "constants.h"

//utility functions
float mutate_s(float point1, float point2, float s) {
    return (point1 + (point2 - point1) * s);
}

float mutate_x(float point1, float point2, float s, float x) {
    return (point1 + x + (point2 - x - point1) * s);
}

float scaling(float k) {
    return ((k + 1.0f) / 2.0f);
}

float in_out_f(float i){
    if (i < 0.5f) {
        return 2 * i * i;
    } else {
        return -2 * i * i + 4 * i - 1;
    }
}

float animate(float transformation, int dir){
  return fmod(1.0f + transformation + dir * TRANSFORMATION_SPEED, 1.0f);
}

bool toggle(bool flag)
{
  return !flag;
}
#pragma once

#include "constants.h"

//utility functions
float mutate_s(float point1, float point2, float s);

float mutate_x(float point1, float point2, float s, float x);

float scaling(float k);

float in_out_f(float i);

float animate(float transformation, int dir);

bool toggle(bool flag);
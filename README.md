# Tesseract(graphics)

An animation of the rotating tesseract(4-dimentional hypercube).

Inspired by video

[Things to See and Hear in the Fourth Dimension - with Matt Parker](https://www.youtube.com/watch?v=1wAaI_6b9JE)

***

Project done using GLUT toolkit for OpenGL

Almost all the maths especially rotation equations were aquired from the Internet.
I'm surely not that smart :D

|File|Description|
|----|:---------:|
|constants.h|A file that contains defined constants for the project|
|tesseract.h|A header file that contains declarations for helper(utility) functions|
|tesseract.cpp|A source file that contains deffinitions(implementations) of helper(utility) functions declared in tesseract.h|
|main.cpp|Asource file that conains all main functionality, functions fot initialization, variables setup, render and display functionality. Has functionality for registring keys (key "A" for animation freeze toggle and "Esc" to quit).|
|Makefile|Contains simple make instruction|


To run the project(Linux)
```
$ make tesseract
$ ./tesseract
```

Alternatively(Linux)
```
$ g++ tesseract.cpp -c
$ g++ main.cpp tesseract.o -o tesseract -lGL -lGLU -lglut
$ ./tesseract
```

Alternatively(Windows)
```
tesseract.cpp -c
g++ main.cpp tesseract.o -o tesseract.exe -lGL -lGLU -lglut
tesseract.exe
```

OR use a compiler shipped with an IDE :D